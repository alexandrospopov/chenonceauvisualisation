var json = []

var openFile = function(event) {
  var input = event.target;

  var reader = new FileReader();
  reader.onload = function(){
    const dataURL = reader.result;
    json = JSON.parse( dataURL )
    document.getElementById("loadingDataModal").style.display = "none"; 
    startDataRendering( json )
    
  };
  reader.readAsText(input.files[0]);
};

// Get the modal
var modal = document.getElementById("loadingDataModal");

// Get the button that opens the modal
var btn = document.getElementById("loadDataButton");

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks on the button, open the modal
btn.onclick = function() {
  window.location.reload();
}

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
  modal.style.display = "none";
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
  if (event.target == modal) {
    modal.style.display = "none";
  }
} 