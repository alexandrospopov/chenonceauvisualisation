
var json = []

const mainTable = d3.select( "#mainTable" );
mainTable.style( "background-color" , "#FFFAF0" ); 

const secondaryTableLH = d3.select( "#secondaryTableLH" );
const secondaryTableRH = d3.select( "#secondaryTableRH" );
secondaryTableLH.style( "background-color" , "#FFFAF0" ); 
secondaryTableRH.style( "background-color" , "#FFFAF0" ); 

const width = +mainTable.attr("width");
const height = +mainTable.attr("height");

const width2 = +secondaryTableLH.attr("width");
const height2 = +secondaryTableLH.attr("height");

let mainTableClicked = false
let secondTableClicked = false

var mainTableCanvas = mainTable.append( 'g' );

mainTableCanvas.append("g").attr("class","yaxis");
mainTableCanvas.append("g").attr("class","xaxis");

var secondTableCanvasLH = secondaryTableLH.append( 'g' );
var secondTableCanvasRH = secondaryTableRH.append( 'g' );

secondTableCanvasLH.append("g").attr("class","yaxis");
secondTableCanvasLH.append("g").attr("class","xaxis");

secondTableCanvasRH.append("g").attr("class","yaxis");
secondTableCanvasRH.append("g").attr("class","xaxis");

var secondTableCanvas = secondTableCanvasRH;


const tooltip = d3.select("body").append("div")	
    .attr("class", "tooltip")				
    .style("opacity", 0);


function renderMainTable( dataTypesAndStages )
{
  arrayDataTypes = dataTypesAndStages.dataType
  arrayStages = dataTypesAndStages.stages

  const sizeElement = 50;

  const margin = { top : 140, right : 20, bottom : 20, left : 200 };

  const xScale = d3.scalePoint()
                   .domain( arrayStages )
                   .range( [ 0, 55 * arrayStages.length ] );
  
  const yScale = d3.scalePoint()
                   .domain( arrayDataTypes )
                   .range( [ 0, 55 * arrayDataTypes.length ] );

  const crossedData = d3.cross( arrayStages, arrayDataTypes );

  const yAxis = d3.axisLeft( yScale );
  const xAxis = d3.axisTop( xScale );

  mainTableCanvas.attr( "transform", `translate(${margin.left},${margin.top})`);  


  mainTableCanvas.select(".yaxis") .call( yAxis )
  mainTableCanvas.select(".xaxis") .call( xAxis )

  mainTableCanvas.selectAll(".xaxis text").attr("transform", function(d) {
      return "translate(" + this.getBBox().height *3 + "," +  this.getBBox().height*-3 + ")rotate(-60)";
  });

  mainTableCanvas.selectAll( "rect" ).data( crossedData )
                         .enter().append( "rect" )
                         .attr("x", d => xScale( d[ 0 ] ) )
                         .attr("y", d => yScale( d[ 1 ] ) )
                         .attr('width', sizeElement)
                         .attr("height",  sizeElement )
}


function keepOnly( process, nameHemisphere)
{
  if ( process.info[0] == null ){
    return process 
  }
  let keys = Object.keys( process.info[0] )

  if ( keys[0].includes('BlockFace' ) )
  {
    // BlockFace does not have hemispheres, therefore the following filtering 
    // does not make sense.
    return process
  }
  
  let keysChosenHemisphere = keys.filter( nameDataProxy =>  nameDataProxy.includes( nameHemisphere ) )

  let processChosenHemisphere = keysChosenHemisphere.reduce( ( obj, nameDataProxy ) => { 
    obj[nameDataProxy] = process.info[0][nameDataProxy]; 
    return obj; },
  {} );
  process.info[0] = processChosenHemisphere
  return process

}

function setSizeSecondaryTable( nb )
{
  if ( nb * 40 + 400  < 500 )
  {
    return 500
  }
  else
  {
    return nb * 40 + 400
  }
}


function renderSecondTable( selectedDataTypes, 
                            processesInSelectedStage,
                            chosenHemisphereName )
{
    
  secondaryTableLH.attr('width', setSizeSecondaryTable( processesInSelectedStage.length ))
  secondaryTableRH.attr('width', setSizeSecondaryTable( processesInSelectedStage.length ))

  const sizeElement = 35;

  const margin2 = { top : 700 , right : 50, bottom : 20, left : 100 };

  const xScale = d3.scalePoint()
                   .domain( processesInSelectedStage.map( d => d.name) )
                   .range([ 0, 40 * processesInSelectedStage.length  ] );
  
  const yScale = d3.scalePoint()
                   .domain( selectedDataTypes )
                   .range([ 0, 40 * selectedDataTypes.length ] );
  let processesInSelectedStageInChosenHemisphere = JSON.parse( JSON.stringify( processesInSelectedStage) ) 
  

  processesInSelectedStageInChosenHemisphere.map( process => keepOnly( process, 
                                                       chosenHemisphereName ) ) 

  const crossedData = d3.cross( processesInSelectedStageInChosenHemisphere.map( d => d.name),
                                selectedDataTypes );


  const yAxis = d3.axisLeft( yScale );
  const xAxis = d3.axisTop( xScale );

  if ( chosenHemisphereName == "RightHemisphere" )
  {
    secondTableCanvas = secondTableCanvasRH;
  }  
  else
  {
    secondTableCanvas = secondTableCanvasLH;
  }

  secondTableCanvas.attr( "transform", `translate(${margin2.left},${margin2.top})`);  

  secondTableCanvas.select(".yaxis") .call( yAxis )
  secondTableCanvas.select(".xaxis") .call( xAxis )

  secondTableCanvas.selectAll(".xaxis text").attr("transform", function() {
      return "translate(" + this.getBBox().height*11 + "," +  this.getBBox().height*-15 + ")rotate(-60)";
  });

  secondTableCanvas.selectAll(".xaxis text").style("font-size", "16px" )
  var carre = secondTableCanvas.selectAll( "rect" ).data( crossedData );

  carre.enter().append( "rect" )
      .attr( "x", d => xScale( d[ 0 ] ) )
      .attr( "y", d => yScale( d[ 1 ] ) )
      .attr( 'width', sizeElement)
      .attr( "height",  sizeElement )
      .attr( "class", "secondTableRect")
      .attr( "fill", d => getColor( d, processesInSelectedStageInChosenHemisphere ))
      .attr( "opacity", "0" );

  secondTableCanvas.selectAll("rect").transition().duration( 300 ).style("opacity",'1')
  
  return processesInSelectedStageInChosenHemisphere
}

function getColor( d, processesInSelectedStage ){
  process = processesInSelectedStage.filter( p => p.name == d[0] );
  let processForDataType = null;

  for ( let dataType in process[0]['info'][0]){

    if ( dataType.includes( d[ 1 ] ) ){

      processForDataType = process[ 0 ][ 'info' ][ 0 ][ dataType ];  

      if ( processForDataType[ "path-existence" ] == 1 ){ return "green" }
      else { return "red" }
    }
  }
}

function interactionSecondTable( processesInSelectedStageInChosenHemisphere,
                                 chosenHemisphere )
{

  let cells = secondTableCanvasLH.selectAll("rect");
  if (chosenHemisphere == "RightHemisphere")
  {
    cells = secondTableCanvasRH.selectAll("rect");
  }

  cells.on('mouseover', dataItemAtProcess => { 
    if ( !secondTableClicked ){
      d3.selectAll(".customParagraph").remove()
      showAccessorsToPath( dataItemAtProcess, processesInSelectedStageInChosenHemisphere ) 

    }

    tooltip.transition()		
    .duration(200)		
    .style("opacity", .9);		
    
    tooltip.html( dataItemAtProcess[0] + "<br/>"  + dataItemAtProcess[1])	
    .style("left", (d3.event.pageX) + "px")		
    .style("top", (d3.event.pageY - 28) + "px");	
    } );


  cells.on('mouseout', () => {
    if (!secondTableClicked){
    d3.selectAll(".customParagraph").remove()
    cells.attr( "fill", e => getColor( e, processesInSelectedStageInChosenHemisphere ))
    } 
    tooltip.transition()		
    .duration(100)		
    .style("opacity", 0);	
    } )


  cells.on('click', dataItemAtProcess => {
    secondTableClicked = !secondTableClicked;
    if (secondTableClicked){
      cells.filter( e => e == dataItemAtProcess )
           .attr("fill","blue")  
    }
  })

}

function interactionMainTable( processAdvancement )
{

  mainTableCanvas.selectAll('rect')

    .on('mouseover', dataTypeAtStage => {
      if ( !mainTableClicked ){
        eraseSecondTable();
        triggerSecondTable( dataTypeAtStage, processAdvancement );
        mainTableCanvas.selectAll('rect').filter( e => e == dataTypeAtStage )
          .attr("fill","blue")  } } )

    .on('click', () => mainTableClicked = !mainTableClicked )

    .on('mouseout', () => {
        if ( !mainTableClicked ) {
          mainTableCanvas.selectAll("rect").attr('fill','black') } } ) 
}

function showAccessorsToPath( dataItemAtProcess, processesInSelectedStage )
{
  process = processesInSelectedStage.filter( p => p.name == dataItemAtProcess[0] );
  let processForDataType = null;
  for ( let outputItem in process)
  {

    for ( let dataType in process[0]['info'][outputItem]){

      if ( dataType.includes( dataItemAtProcess[ 1 ] ) )
      {
        processForDataType = process[outputItem]['info'][0][ dataType ];  
        typeAcqs = Object.keys( process[outputItem]['info'][0][ dataType ][ 'path-dictionary' ] )
        typeAcqs.sort()
        for( numAcq = 0; numAcq < typeAcqs.length; numAcq++ )
        {
          d3.select('#infoDiv')
            .append("button")
            .attr( "class","customParagraph")
            .attr( "value", process[outputItem]['info'][0][ dataType ][ 'path-dictionary' ][ typeAcqs[ numAcq ] ] ) 
            .attr( "onclick", "copyToClipBoard( this.value )" )
            .text( typeAcqs[ numAcq ] )
        }
       
        break;
      }
    }
  }


}

function copyToClipBoard( val )
{
  navigator.clipboard.writeText(val)
  .then(() => {
    console.log("copied " + val + " to clipboard.")
  })
  .catch(err => {
    console.log('Something went wrong', err);
  });
  
}


function eraseSecondTable()
{
  secondTableCanvasLH.selectAll( "rect" ).data( [] ).exit().remove() ;
  secondTableCanvasRH.selectAll( "rect" ).data( [] ).exit().remove() ;
}


function triggerSecondTable( dataTypeAtStage, processAdvancement )
{

  const selectedStage = dataTypeAtStage[ 0 ]
  const selectedDataType = dataTypeAtStage[ 1 ]


  Promise.all([ d3.json( "existingDataItemPerDataType.json" ) ]).then(function( existingDataItemPerDataType ) {
    const dataItemPerDataType = existingDataItemPerDataType[ 0 ]
    const allProcesses = processAdvancement

    const processesInSelectedStage = allProcesses
                .filter( d => d.name.includes( selectedStage ) )
                .filter( d => d.outputDataType.includes( selectedDataType ) );     

    let dataItemsCorrespondingToProcesses = [];

    for ( let dataType in dataItemPerDataType){
      if ( dataType == selectedDataType ){
        dataItemsCorrespondingToProcesses = dataItemPerDataType[ dataType ];  
        break;
      }
    }
    processesInSelectedStageLH = renderSecondTable( 
                        dataItemsCorrespondingToProcesses['LeftHemisphere'], 
                       processesInSelectedStage,
                       "LeftHemisphere" )
    processesInSelectedStageRH = renderSecondTable( 
                        dataItemsCorrespondingToProcesses['RightHemisphere'], 
                        processesInSelectedStage,
                        "RightHemisphere" )


    processesInSelectedStageLH = interactionSecondTable( processesInSelectedStageLH, 
                                                         "LeftHemisphere" )
    processesInSelectedStageRH = interactionSecondTable( processesInSelectedStageRH, 
                                                         "RightHemisphere" )


})

}

var startDataRendering = function( processAdvancement )
{

d3.json( "existingDataTypesAndStages.json" ).then( 
  dataTypesAndStages => { 
    renderMainTable( dataTypesAndStages );
    interactionMainTable( processAdvancement );
  }
)
}